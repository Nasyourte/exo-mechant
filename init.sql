drop database if exists mechant;
create database mechant;
use mechant;
GRANT ALL PRIVILEGES on mechant.* TO korwen@127.0.0.1; -- a changer si votre user s'appelle pas toto

create table mechant (
    id int auto_increment primary key,
    nom varchar(255),
    pouvoir text,
    imgURL varchar(255)
);

insert into mechant(nom, pouvoir, imgURL) values ('maxime','git', 'http://www.filmfad.com/wp-content/uploads/2015/09/keanu-reeves-bw-FilmFad.com_.jpg');