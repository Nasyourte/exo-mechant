<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


include 'bdd-pwd.php';
$host = '127.0.0.1';
$db   = 'liste';

$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);


function addMechant($nom, $pouvoir, $image){
    global $pdo;
    $req = $pdo->prepare("
        insert into mechant(nom, pouvoir, imgURL) 
                    values (?,   ?,       ?);
    ");
    $req->execute([$nom, $pouvoir, $image]);
}

function getAllMechant(){
    $req = $pdo->query('select * from mechant;');
    return $req->fetchAll();
}

function getMechant($id){
    $req = $pdo->prepare('select * from mechant where id=?;');
    $req->execute([$id]);
    return $req->fetchAll();
}

function supprMechant($id){
    $req = $pdo->prepare('delete from mechant where id=?;');
    $req->execute([$id]);
}

function updateMechant($id, $nom, $pouvoir, $image){
    $req = $pdo->prepare("
        update mechant set
        nom = ?,
        pouvoir = ?,
        imgURL = ?
        where id=?;
    ");
    $req->execute([$nom, $pouvoir, $image, $id]);
}