<?php

include("data.php");

$id = $_GET("id");

$mechant = getMechant($id);

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $nom = $_POST["nom"];
    $pouvoir = $_POST["pouvoir"];
    $image = $_POST["imgURL"];

    updateMechant($id, $nom, $pouvoir, $image);

    header('Location: index.php');
}

?>